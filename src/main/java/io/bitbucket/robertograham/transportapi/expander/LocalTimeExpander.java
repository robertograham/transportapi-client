package io.bitbucket.robertograham.transportapi.expander;

import feign.Param;
import lombok.Value;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Value
public class LocalTimeExpander implements Param.Expander {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

    @Override
    public String expand(final Object value) {
        if (value == null)
            return null;
        if (value instanceof LocalTime) {
            return FORMATTER.format((LocalTime) value);
        }
        throw new IllegalArgumentException("value was not null or an instance of LocalTime");
    }
}