package io.bitbucket.robertograham.transportapi.expander;

import feign.Param;
import lombok.Value;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Value
public class LocalDateExpander implements Param.Expander {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Override
    public String expand(final Object value) {
        if (value == null)
            return null;
        if (value instanceof LocalDate) {
            return FORMATTER.format((LocalDate) value);
        }
        throw new IllegalArgumentException("value was not null or an instance of LocalDate");
    }
}