package io.bitbucket.robertograham.transportapi;

import feign.Request;
import feign.RequestTemplate;
import feign.Target;
import lombok.NonNull;
import lombok.Value;

@Value
class TransportApiRequestAuthenticatorTarget implements Target<TransportApi> {

    @NonNull
    String applicationId;

    @NonNull
    String applicationKey;

    @Override
    public Class<TransportApi> type() {
        return TransportApi.class;
    }

    @Override
    public String name() {
        return "transportApi";
    }

    @Override
    public String url() {
        return "https://transportapi.com/v3/uk";
    }

    @Override
    public Request apply(final RequestTemplate requestTemplate) {
        return requestTemplate.target(url())
            .query("app_id", applicationId)
            .query("app_key", applicationKey)
            .request();
    }
}
