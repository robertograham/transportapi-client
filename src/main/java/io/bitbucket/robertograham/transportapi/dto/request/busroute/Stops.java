package io.bitbucket.robertograham.transportapi.dto.request.busroute;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Stops {

    ALL("all"), ONWARD("onward");

    @Getter
    @NonNull
    private final String value;

    @Override
    public String toString() {
        return value;
    }
}
