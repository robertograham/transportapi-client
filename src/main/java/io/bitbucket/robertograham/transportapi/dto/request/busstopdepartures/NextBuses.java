package io.bitbucket.robertograham.transportapi.dto.request.busstopdepartures;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum NextBuses {

    YES("yes"), NO("no");

    @Getter
    @NonNull
    private final String value;

    @Override
    public String toString() {
        return value;
    }
}
