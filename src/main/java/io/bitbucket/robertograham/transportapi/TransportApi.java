package io.bitbucket.robertograham.transportapi;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import feign.Feign;
import feign.Logger;
import feign.Param;
import feign.RequestLine;
import feign.jackson.JacksonDecoder;
import feign.slf4j.Slf4jLogger;
import io.bitbucket.robertograham.transportapi.dto.request.busroute.Stops;
import io.bitbucket.robertograham.transportapi.dto.request.busstopdepartures.Group;
import io.bitbucket.robertograham.transportapi.dto.request.busstopdepartures.NextBuses;
import io.bitbucket.robertograham.transportapi.dto.response.BusRouteResponse;
import io.bitbucket.robertograham.transportapi.dto.response.BusServiceResponse;
import io.bitbucket.robertograham.transportapi.dto.response.BusStopDeparturesResponse;
import io.bitbucket.robertograham.transportapi.expander.LocalDateExpander;
import io.bitbucket.robertograham.transportapi.expander.LocalTimeExpander;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

public interface TransportApi {

    static TransportApi newTransportApi(final String applicationId, final String applicationKey) {
        return Feign.builder()
            .logger(new Slf4jLogger())
            .logLevel(Logger.Level.FULL)
            .decoder(new JacksonDecoder(Set.of(new JavaTimeModule())))
            .target(new TransportApiRequestAuthenticatorTarget(applicationId, applicationKey));
    }

    @RequestLine("GET /bus/services/{operator}:{line}.json")
    BusServiceResponse busService(@Param("operator") String operator,
                                  @Param("line") String line);

    @RequestLine("GET /bus/stop/{atcoCode}/live.json?group={group}&limit={limit}&nextbuses={nextBuses}")
    BusStopDeparturesResponse busStopDepartures(@Param("atcoCode") String atcoCode,
                                                @Param("group") Group group,
                                                @Param("limit") Integer limit,
                                                @Param("nextBuses") NextBuses nextBuses);

    @RequestLine("GET /bus/stop/{atcoCode}/{date}/{time}/timetable.json?group={group}&limit={limit}")
    BusStopDeparturesResponse busStopDepartures(@Param("atcoCode") String atcoCode,
                                                @Param(value = "date", expander = LocalDateExpander.class) LocalDate date,
                                                @Param(value = "time", expander = LocalTimeExpander.class) LocalTime time,
                                                @Param("group") Group group,
                                                @Param("limit") Integer limit);

    @RequestLine("GET /bus/route/{operator}/{line}/{direction}/{atcoCode}/{date}/{time}/timetable.json?edge_geometry={edgeGeometry}&stops={stops}")
    BusRouteResponse busRoute(@Param("operator") String operator,
                              @Param("line") String line,
                              @Param("direction") String direction,
                              @Param("atcoCode") String atcoCode,
                              @Param(value = "date", expander = LocalDateExpander.class) LocalDate date,
                              @Param(value = "time", expander = LocalTimeExpander.class) LocalTime time,
                              @Param("edgeGeometry") Boolean edgeGeometry,
                              @Param("stops") Stops stops);
}
