package io.bitbucket.robertograham.transportapi;

import io.bitbucket.robertograham.transportapi.dto.request.busstopdepartures.NextBuses;
import io.bitbucket.robertograham.transportapi.dto.response.BusServiceResponse;
import io.bitbucket.robertograham.transportapi.dto.response.BusStopDeparturesResponse;
import org.junit.jupiter.api.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

class TransportApiTest {

    @Test
    void newTransportApi() {
        assertThrows(
            NullPointerException.class,
            () -> TransportApi.newTransportApi(null, ""),
            "Did not throw NPE when applicationKey was null"
        );
        assertThrows(
            NullPointerException.class,
            () -> TransportApi.newTransportApi("", null),
            "Did not throw NPE when applicationId was null"
        );
        assertNotNull(
            TransportApi.newTransportApi("", ""),
            "Did not return a TransportApi instance"
        );
    }

    @Test
    void busService() {
        final TransportApi transportApi = createTransportApi();
        final String operatorCode = "SL";
        final BusServiceResponse busServiceResponse = transportApi.busService(operatorCode, "59");
        assertEquals(operatorCode, busServiceResponse.getOperator().getCode());
    }

    @Test
    void busStopDepartures() {
        final TransportApi transportApi = createTransportApi();
        final String atcoCode = "490000077E";
        final BusStopDeparturesResponse busStopDeparturesResponse = transportApi.busStopDepartures(atcoCode, null, null, NextBuses.NO);
        assertEquals(atcoCode, busStopDeparturesResponse.getAtcoCode());
    }

    @Test
    void busStopDeparturesWithDateAndTime() {
        final TransportApi transportApi = createTransportApi();
        final String atcoCode = "490000077E";
        final BusStopDeparturesResponse busStopDeparturesResponse = transportApi.busStopDepartures(atcoCode, LocalDate.now(), LocalTime.now(), null, null);
        assertEquals(atcoCode, busStopDeparturesResponse.getAtcoCode());
    }

    private TransportApi createTransportApi() {
        try (final InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("transportapi-credentials.yml")) {
            final Yaml yaml = new Yaml();
            final TransportApiCredentials transportApiCredentials = yaml.loadAs(inputStream, TransportApiCredentials.class);
            return TransportApi.newTransportApi(transportApiCredentials.getApplicationId(), transportApiCredentials.getApplicationKey());
        } catch (final IOException exception) {
            return TransportApi.newTransportApi("", "");
        }
    }
}