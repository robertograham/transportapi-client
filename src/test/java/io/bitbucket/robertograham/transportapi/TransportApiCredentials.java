package io.bitbucket.robertograham.transportapi;

import lombok.NonNull;
import lombok.Value;

@Value
class TransportApiCredentials {

    @NonNull
    String applicationId;

    @NonNull
    String applicationKey;
}
